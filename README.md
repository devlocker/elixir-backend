# DevLocker

To run the application:

1. Install dependencies with `mix deps.get`
2. Start Phoenix endpoint with `mix phoenix.server`

Now you can visit `localhost:4000` from your browser.

Clients that have been built (mostly):
- Github
- Basecamp
