defmodule DevLocker.Basecamp.Client do
  """
  Public Interface
  """

  """
  Returns a List of the uncompleted todos for a particular todolist.
  [todo1, todo2, todo3, ...]
  """
  def todos(todolist_id) do
    HTTPotion.get(todolist_urls[todolist_id] <> "/todos/remaining.json", [headers: headers]).body
    |> Poison.decode!
    |> case do
         %{"error" => error} -> error
         todos -> todos
       end
  end

  """
  Marks a todo's completed status to true, removing it from the todos list.
  %Todo{}
  """
  def complete_todo(todo_id, todolist_id) do
    body = Poison.encode!(%{completed: true})
    HTTPotion.put(todolist_urls[todolist_id] <> "/todos/#{todo_id}.json", [body: body, headers: headers]).body
    |> Poison.decode!
  end

  """
  Returns a List of all todolists for the selected projects of an addon.
  [todolist1, todolist2, todolist3, ...]
  """
  def todolists do
    Enum.map(selected_projects, fn (project_id) -> get_todolists(project_id) end)
    |> List.flatten
  end

  """
  PRIVATE METHODS
  """

  """
  Returns a Map of todolist ids going to their api url, minus the .json extension.
  %{
    1 => "https://basecamp.com/<account_id>/api/v1/projects/<project_id>/todolists/1,
    2 => "https://basecamp.com/<account_id>/api/v1/projects/<project_id>/todolists/2
  }
  """
  defp todolist_urls do
    Enum.reduce(todolists, %{}, fn (todolist, acc) ->
      split_url = List.first(String.split(todolist["url"], ".json"))
      Map.put(acc, todolist["id"], split_url)
    end
    )
  end

  """
  Returns a List of all the todolists for a particular project.
  [todolist1, todolist2, todolist3]
  """
  defp get_todolists(project_id) do
    url = project_urls[project_id] <> "/todolists.json"

    HTTPotion.get(url, [headers: headers]).body
    |> Poison.decode!
    |> case do
         %{"error" => error} -> error
         todolists -> todolists
       end
  end

  """
  Returns a Map of project ids going to their api url, minus the .json extension.
  %{
    1 => "https://basecamp.com/<account_id>/api/v1/projects/1,
    2 => "https://basecamp.com/<account_id>/api/v1/projects/2
  }
  """
  defp project_urls do
    Enum.reduce(projects, %{}, fn (project, acc) ->
      split_url = List.first(String.split(project["url"], ".json"))
      Map.put(acc, project["id"], split_url)
    end
    )
  end

  """
  Returns a List of all the projects for all of the accounts a user has access to.
  [project1, project2]
  """
  defp projects do
    Enum.map(get_accounts, fn (account) -> get_projects(account) end)
    |> List.flatten
  end

  """
  Returns a List of all the projects for a particular Basecamp account"
  """
  defp get_projects(account) do
    HTTPotion.get(account["href"] <> "/projects.json", [headers: headers]).body
    |> Poison.decode!
    |> case do
         %{"error" => error} -> error
         projects -> projects
       end
  end

  """
  Returns a List of all the accounts a user has access to.
  """
  defp get_accounts do
    HTTPotion.get(accounts_url, [headers: headers]).body
    |> Poison.decode!
    |> case do
         %{"error" => error} -> error
         %{"accounts" => accounts} -> accounts
       end
  end

  defp accounts_url do
    "https://launchpad.37signals.com/authorization.json"
  end

  defp headers do
    ["User-Agent": "DevLocker", "Authorization": "Bearer #{api_token}", "Content-Type": "application/json"]
  end

  defp api_token do
    # addon.api_token
    "BAhbByIBsHsiZXhwaXJlc19hdCI6IjIwMTUtMTAtMTBUMjA6MjE6NTdaIiwidmVyc2lvbiI6MSwidXNlcl9pZHMiOlsyMzgxNjkwM10sImFwaV9kZWFkYm9sdCI6IjQ4ZDE3YTY2N2ZiOTBkZGNjMjhhNmFkZWFiODg4ZTdhIiwiY2xpZW50X2lkIjoiODE2MDAzNzlhZmIwNTAxNjhhOTBiMmUzNDliZjg5MWE4ODU4Njg1OCJ9dToJVGltZQ1U5RzA+cycVw==--e9bc4d27fb6f9e1460b8eaf94485b5be275edb0a"
  end

  defp selected_projects do
    # addon.metadata["selected_projects"]
    [9729710]
  end
end
