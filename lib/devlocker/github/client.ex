defmodule DevLocker.Github.Client do
  """
  Returns a list of all the pulls the user has access to
  [pull1, pull2, pull3...]
  """
  def pulls do
    receiving_pid = self
    repos
    |> Enum.map(fn (repo) ->
      spawn_link fn -> (send receiving_pid, { self, repo_pulls(repo) }) end
    end)
    |> Enum.map(fn (pid) ->
      receive do { ^pid, result } -> result end
    end)
    |> List.flatten
  end

  """
  Returns a list of hashes, with the repo name and a list of pulls
  [%{name: "repo_name", pulls: [pull1, pull2]}]
  """
  def pulls_with_repo do
    receiving_pid = self
    repos
    |> Enum.map(fn (repo) ->
      spawn_link fn -> (send receiving_pid, { self, %{name: repo["name"], pulls: repo_pulls(repo)} }) end
    end)
    |> Enum.map(fn (pid) ->
      receive do { ^pid, result } -> result end
    end)
    |> List.flatten
  end

  """
  PRIVATE METHODS
  """

  defp repo_pulls(repo) do
    owner = repo["owner"]["login"]
    repo_name = repo["name"]

    HTTPotion.get(pulls_url(owner, repo_name), headers).body
    |> Poison.decode!
  end

  defp repos, do: _repos(repos_url, [])

  defp _repos(nil, acc), do: List.flatten(acc)
  defp _repos(url, acc) do
    response = HTTPotion.get(url, headers)

    result = acc ++ [Poison.decode!(response.body)]

    if Regex.run(~r/next/, response.headers[:Link]) do
      Regex.run(~r/\<(\S+)\>/, response.headers[:Link])
      |> List.last
      |> _repos(result)
    else
      _repos(nil, result)
    end
  end

  defp headers do
    [headers: ["User-Agent": "DevLocker", "Authorization": "token #{token}", "Accept": "application/vnd.github.v3+json"]]
  end

  defp pulls_url(owner, repo) do
    base_url <> "/repos/#{owner}/#{repo}/pulls"
  end

  defp repos_url do
    base_url <> "/user/repos"
  end

  defp base_url do
    "https://api.github.com"
  end

  defp token do
    # "put a real token here - but don't commit it, otherwise Github will void it"
    "2609deb679c346ae8aa71a07ab8be5b1377f4a69"
  end
end
