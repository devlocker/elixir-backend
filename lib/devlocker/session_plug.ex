defmodule DevLocker.SessionPlug do
  import Plug.Conn
  use Phoenix.Controller

  @behaviour Plug

  """
  Necessary for a Plug to have an init/1 method, but it's never used.
  Anyways, just pass on the arg
  """
  def init(opts) do
    opts
  end

  """
  If the user is signed in, return the connection and let them through.
  If the user is not logged in, redirect to /sign_in and halt the connection
  """
  def call(conn, _opts) do
    case signed_in?(conn) do
      {:ok, conn} -> conn |> set_current_user
      {:error, conn} -> conn |> redirect(to: "/sign_in") |> halt
    end
  end

  """
  Check if a :user_id is present.
  The :user_id might come back as nil, "", or an Integer, so we convert to string
  and check the length to tell if it's set or not
  """
  defp signed_in?(conn) do
    get_user_id(conn)
    |> is_signed_in?(conn)
  end

  defp is_signed_in?(user_id, conn) when is_integer(user_id), do: {:ok, conn}
  defp is_signed_in?(_, conn), do: {:error, conn}

  defp set_current_user(conn) do
    user = DevLocker.Repo.get(DevLocker.User, get_user_id(conn))

    Plug.Conn.assign(conn, :current_user, user)
  end

  defp get_user_id(conn) do
    get_session(conn, :user_id)
  end
end
