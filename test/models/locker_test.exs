defmodule DevLocker.LockerTest do
  use DevLocker.ModelCase

  alias DevLocker.Locker

  @valid_attrs %{name: "some content", user: nil}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Locker.changeset(%Locker{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Locker.changeset(%Locker{}, @invalid_attrs)
    refute changeset.valid?
  end
end
