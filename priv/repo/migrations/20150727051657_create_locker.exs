defmodule DevLocker.Repo.Migrations.CreateLocker do
  use Ecto.Migration

  def change do
    create table(:lockers) do
      add :name, :string
      add :user_id, :integer

      timestamps
    end
    create index(:lockers, [:user_id])

  end
end
