defmodule DevLocker.LayoutView do
  use DevLocker.Web, :view

  def current_user(conn) do
    DevLocker.Repo.get(DevLocker.User, current_user_id(conn))
  end

  def current_user_id(conn) do
    case user_id = conn.private.plug_session["user_id"] do
      is_integer -> user_id
      _ -> nil
    end
  end
end
