defmodule DevLocker.LockerController do
  use DevLocker.Web, :controller

  alias DevLocker.Locker

  plug DevLocker.SessionPlug
  plug :scrub_params, "locker" when action in [:create, :update]

  def index(conn, _params) do
    lockers = Repo.all(Locker)
    render(conn, "index.html", lockers: lockers)
  end

  def new(conn, _params) do
    changeset = Locker.changeset(%Locker{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"locker" => locker_params}) do
    locker_params = Map.put(locker_params, "user_id", get_session(conn, :user_id))
    changeset = Locker.changeset(%Locker{}, locker_params)

    if changeset.valid? do
      Repo.insert!(changeset)

      conn
      |> put_flash(:info, "Locker created successfully.")
      |> redirect(to: locker_path(conn, :index))
    else
      render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    locker = Repo.get!(Locker, id)
    render(conn, "show.html", locker: locker)
  end

  def delete(conn, %{"id" => id}) do
    locker = Repo.get!(Locker, id)
    Repo.delete!(locker)

    conn
    |> put_flash(:info, "Locker deleted successfully.")
    |> redirect(to: locker_path(conn, :index))
  end
end
