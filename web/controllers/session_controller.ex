defmodule DevLocker.SessionController do
  use DevLocker.Web, :controller

  def new(conn, _params) do
    changeset = DevLocker.User.changeset(%DevLocker.User{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => params}) do
    user = auth_user(params)

    if user do
      conn = put_session(conn, :user_id, user.id)

      redirect conn, to: locker_path(conn, :index)
    else
      conn
      |> put_flash(:notice, "Invalid email or password")
      |> redirect(to: "/sign_in")
    end
  end

  def delete(conn, _params) do
    conn = put_session(conn, :user_id, nil)

    redirect conn, to: "/sign_in"
  end

  defp auth_user(%{"email" => email, "password" => password}) do
    Repo.get_by(DevLocker.User, email: email)
    |> password_match?(password)
  end
  defp auth_user(_params), do: nil

  defp password_match?(user, password) do
    if user.password == password do
      user
    else
      nil
    end
  end
end
