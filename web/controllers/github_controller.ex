defmodule DevLocker.GithubController do
  use DevLocker.Web, :controller
  plug DevLocker.SessionPlug

  def index(conn, _params) do
    pulls = client.pulls_with_repo

    render(conn, "index.html", pulls: pulls)
  end

  defp client do
    DevLocker.Github.Client
  end
end
