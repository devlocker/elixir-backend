defmodule DevLocker.UserController do
  use DevLocker.Web, :controller

  alias DevLocker.User

  plug :scrub_params, "user" when action in [:create, :update]

  def new(conn, _params) do
    changeset = User.changeset(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.changeset(%User{}, user_params)

    if changeset.valid? do
      Repo.insert!(changeset)

      conn
      |> put_flash(:info, "Sign up successful")
      |> redirect(to: locker_path(conn, :index))
    else
      render(conn, "new.html", changeset: changeset)
    end
  end
end
