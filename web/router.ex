defmodule DevLocker.Router do
  use DevLocker.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DevLocker do
    pipe_through :browser # Use the default browser stack

    get "/", LockerController, :index
    get "/sign_in", SessionController, :new
    delete "/sign_out", SessionController, :delete
    resources "/sessions", SessionController
    resources "/users", UserController
    resources "/lockers", LockerController
  end

  # Other scopes may use custom stacks.
  # scope "/api", DevLocker do
  #   pipe_through :api
  # end
end
