defmodule DevLocker.User do
  use DevLocker.Web, :model

  schema "users" do
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :password, :string

    has_many :lockers, DevLocker.Locker
    timestamps
  end

  @required_fields ~w(first_name last_name email password)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def full_name(user) do
    user.first_name <> " " <> user.last_name
  end
end
